# noinspection SqlNoDataSourceInspectionForFile


-- Insert values in PROPERTY_OWNER table.
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('123456789','Katerina','Papaioannou','6981796383','kspapaioannou@gmail.com','$2a$10$EOWc5gXu2.lpLQ.DeVzqcOu6.xhURcW.ApxsOh.78UEIYRQkSO7cW','USER'); #123456
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('012345678','Anastasia','Tsatsou','6974586838','tsatsou@gmail.com','$2a$10$gx8VkE233h5agE9pAG.toOacm9ZH9SUAFCNwA1z7jrzrGxSwZMJ3m','USER'); #0123456
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('987654321','Thomas','Giannos','6999999999','thomas@gmail.com','$2a$10$HW.2lYvxgRakr1lsT30tyuOz5jpmA8QmbHMAI57v4z7WdkjEYYhIy','USER'); #654321
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('876543210','Stelios','Kokkalis','6900000000','stelios@gmail.com','$2a$10$veiBxkNpUO/ET9QaxrICCOlOOB8vgaT7cT3CFi1LwvsyHKH7ZSe/G','USER'); #000000
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('816489573','Maria','Pappa','6981234567','m.pappa@gmail.com','$2a$10$EWAw/lAxnci2/dCo85GyeOn8Ttw3yKGzUms3DdQrRXLVR1VW7gcAy','USER'); #111111
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('235794658','Anna-Maria','Brati','6982345678','a.brati@gmail.com','$2a$10$tFZnFXR7QYKyDooxn5xdl.o2z2PUA9mHEBz1EEvXfcSI/zbiylxlS','USER'); #kodikos1234
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('123589467','Giwrgos','Iwannou','6973251437','g.iwannou@gmail.com','$2a$10$fJPbMZ6a2skWRUp8MoqBkOrm4o2Lto4VMJpwnIHZUhM1J2RIHzSte','USER'); #"guest"
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('524893574','Kwnstantinos','Kritikos','69933667777','k.kritikos@gmail.com','$2a$10$60P4FfKntLF8wkYz3lLVLO2.wDlcbXQQp2jALBg0V9vJslfuAtwZm','USER'); #root
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('541587635','Vasiliki','Papagewrgiou','6905150298','v.papage@gmail.com','$2a$10$7c1LqtIX0uNpVywwMH81re6ZtQQFvNnXvRy6nQGxRPiBIir1J44MG','USER'); #toor123
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('254687413','Christos','Raptis','6905150329','c.raptis@gmail.com','$2a$10$QqWVBOjz9hp7nSE6q9kUfOOIiljGdZ3QR93Ba4r3zG4x/EAEaG5vW','USER'); #password1
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('168475238','Spyros','Aggalakis','6931548324','s.aggalakis13@gmail.com','$2a$10$W0q.VNUu49zV4ZhJchI2YeAg/fRx1bIZVQZtCWUOAogHi6IcDEFWy','USER'); #11071980
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('135789456','Ilias','Kagias','6947512389','ilias.kagias@gmail.com','$2a$10$vZmaDLrwMVPz.geHNRBT3OBb1TckUIx9Mi8nSSLaDYGDy9umdLMBK','USER'); #repairpass
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('144568472','Eirini','Nikolakaki','6974856931','e.nikol@gmail.com','$2a$10$3N.z57ob9JMyTctH/u76Bu8eKx4xupQKYnkYGUHFFT1C4fPIKHTGq','USER'); #mpainw
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('501860487','Panagiwtis','Papamakarios','6961523288','p.pap@gmail.com','$2a$10$wD5IymnPjgjfxu9E71//5OEGpl6nNMUep7XXwDjSooM0DohJ2LWFu','USER'); #"papa1507"
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('015608745','Afroditi','Skoulou','6972530612','afro.skoul@gmail.com','$2a$10$G5GkrHyGYD3Sm/prxyUZlOnB7BkKxLuZtrq9KKogyJemAwg3FTIRW','USER'); #eimaithea1
INSERT INTO `PROPERTY_OWNER` (`owner_SSN`,`first_name`,`last_name`,`phone_number`,`email`,`password`,`role`) VALUES ('488620032','Alexis','Tsipras','6972452768','admin@gmail.com','$2a$10$Nkzof5HChQEGRVIcwiTNle7PV5c0dUK9WWiYbAdiqrg.7.zet6IQe','ADMIN'); #admin


-- Insert values in PROPERTY table
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('123456789','Tzavela','1998','HOUSE','1');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('012345678','Keratsiniou','2004','HOUSE','2');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('152478963','Avgis','1989','APARTMENT','2');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('356842189','Filolaou','2000','MAISONETTE','3');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('159753852','Kimis','1999','HOUSE','4');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('164587942','Alexandras','2001','SHOP','5');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('321654987','Pilou','1997','BLOCK_OF_FLATS','9');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('156324978','Politexniou','1995','APARTMENT','12');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('146325979','Karaiskaki','2005','APARTMENT','15');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('040784510','Kriezi','1987','HOUSE','6');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('235108483','Leoforos Dimokratias','2007','SHOP','1');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('810462096','Athanasiou Diakou','2008','BLOCK_OF_FLATS','10');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('015825018','Riga Feraiou','1985','MAISONETTE','8');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('321431568','Thrasivoulou','2000','MAISONETTE','7');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('235178904','Korai','2014','HOUSE','11');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('142537803','Dionisiou','1979','HOUSE','1');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('019283651','Aristotelous','1965','BLOCK_OF_FLATS','13');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('725196382','Platonos','2004','SHOP','14');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('917406184','Smirnis','1999','HOUSE','3');
INSERT INTO `PROPERTY` (`property_state_id`,`address`,`manufacturing_year`, `type`,`owner_id`) VALUES ('341825480','Nikitara','2006','APARTMENT','15');


-- Insert values in REPAIR table
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-12-22','PENDING','PAINT_JOB',200.00,'2','Paint 2 rooms');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-11-08','COMPLETED','PLUMBING',100.00,'4','Repair broken pipe');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2020-4-19','PENDING','FRAMING',800.00,'1','Change all the framing');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-12-03','COMPLETED','ELECTRICAL_WORK',70.00,'7','Corrections on electrical board');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-09-05','COMPLETED','PAINT_JOB',300.00,'9','Paint outside property');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-11-15','COMPLETED','FRAMING',100.00,'8','Repair broken frame');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-11-29','COMPLETED','ELECTRICAL_WORK',50.00,'12','Repair broken socket');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-10-30','COMPLETED','PAINT_JOB',103.00,'15','Paint a wall');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-12-12','UNDERWAY','FRAMING',200.00,'19','Repair window framing');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-12-18','UNDERWAY','PLUMBING',190.99,'16','Change bathroom pipe');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-11-25','COMPLETED','PAINT_JOB',420.00,'17','Paint 4 rooms');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-11-10','COMPLETED','FRAMING',310.00,'18','Repair 3 doors framing');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2020-01-10','DEFAULT','ELECTRICAL_WORK',180.00,'14','Change all the light bulbs');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2020-01-08','PENDING','DEFAULT',50.00,'13','Change a broken light');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-11-30','UNDERWAY','PAINT_JOB',1000.00,'6','Paint the whole property inside and out ');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-12-03','COMPLETED','ELECTRICAL_WORK',120.00,'16','Repair electric board');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-10-15','COMPLETED','PLUMBING',210.00,'10','Repair bathroom and kitchen pipes');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-12-09','UNDERWAY','PAINT_JOB',225.00,'13','Paint the property outside');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-12-26','PENDING','FRAMING',80.00,'3','Change bedroom widow framing');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-11-18','COMPLETED','FRAMING',280.00,'4','repair kitchen windows framing');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-09-17','COMPLETED','ELECTRICAL_WORK',390.00,'11','Repair all the melted wires in property');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2020-01-27','PENDING','DEFAULT',150.00,'20','Paint bathroom');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2020-01-20','DEFAULT','DEFAULT',360.00,'5','Replace all the sockets');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-12-23','PENDING','PLUMBING',230.00,'6','Replace broken pipes');
INSERT INTO `REPAIR` (`date`,`repair_status`,`repair_type`,`cost`,`property_id`,`description`) VALUES ('2019-12-06','UNDERWAY','PAINT_JOB',740.00,'3','Paint 6 rooms ');
