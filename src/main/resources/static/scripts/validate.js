jQuery(function ($) {
	jQuery.validator.addMethod("isfloat", function(value, element) {
        return this.optional(element) || (/^([1-9][0-9]*\.[0-9]{1,2})$/.test(value) || /^([1-9][0-9]*)$/.test(value));
    }, "* Please enter a valid cost");

    if($('#cost').length > 0){
        $('#cost').val($('#cost').val().replace(",","."));
    }

	$('#login-validation').validate(
		{
			rules:
			{
				username: { required: true, email: true, maxlength: 255},
				password: { required: true }
			},
			messages:
			{
				username: { required: 'Please enter your email', email: 'Please enter a valid email', maxlength: 'Your email cannot have more than 255 characters'},
				password: { required: 'Please enter your password'}
			}
		}
	)

    $('#property-create-validation').validate(
		{
			rules:
				{
					propertyStateId: { required: true, digits: true, minlength: 9, maxlength:9, remote:{url: "/admin/api/create/property/checkStateId", type: "post"}},
					address: {required: true, maxlength: 255},
					manufacturingYear: { minlength: 4, maxlength: 4, min: 1900, max: 2020 },
					ownerSsn: {required: true, digits: true, minlength:9, maxlength: 9, remote:{url: "/admin/api/create/property/checkSSN", type: "post"}}
				},
			messages:
				{
					propertyStateId: { required: 'Please enter the Property State ID', digits: 'ID can only contain digits', minlength: 'ID must be 9 digits long', maxlength: 'ID must be 9 digits long', remote: 'The state ID you provided already exists'},
					address: {required: 'Please enter your address', maxlength: 'Your address cannot have more than 255 characters'},
					manufacturingYear: { minlength: 'Please enter a valid year', maxlength: 'Please enter a valid year',min: 'Please enter a valid year', max: 'Please enter a valid year'},
					ownerSsn: { required: 'Please enter a valid owner SSN', digits: 'SSN can only contain digits', minlength: 'The owner SSN cannot have less than 9 characters', maxlength: 'The owner SSN cannot have more than 9 characters', remote: 'The provided SSN does not exist'}
				}
		}
	);

	$('#property-edit-validation').validate(
		{
			rules:
				{
					address: {required: true, maxlength: 255},
					manufacturingYear: { minlength: 4, maxlength: 4, min: 1900, max: 2020 },
					ownerSsn: {required: true, digits: true, minlength:9, maxlength: 9, remote:{url: "/admin/api/edit/property/checkSSN", type: "post"}}
				},
			messages:
				{
					address: {required: 'Please enter your address', maxlength: 'Your address cannot have more than 255 characters'},
					manufacturingYear: { minlength: 'Please enter a valid year', maxlength: 'Please enter a valid year',min: 'Please enter a valid year', max: 'Please enter a valid year'},
					ownerSsn: { required: 'Please enter a valid owner SSN', digits: 'SSN can only contain digits', minlength: 'The owner SSN cannot have less than 9 characters', maxlength: 'The owner SSN cannot have more than 9 characters', remote: 'The provided SSN does not exist'}
				}
		}
	);

	$('#owner-create-validation').validate(
		{
			rules:
				{
					ssn: { required: true, digits: true, minlength: 9, maxlength: 9, remote:{url: "/admin/api/create/owner/checkSSN", type: "post"} },
					firstName: { required: true, maxlength: 255 },
					lastName: {required: true, maxlength: 255 },
					phoneNumber: {required: true, digits: true},
					email: { required: true, email: true, maxlength: 255, remote:{url: "/admin/api/create/owner/checkEmail", type: "post"}},
					password: { required: true }
				},
			messages:
				{
					ssn: { required: 'Please enter your SSN', digits: 'SSN can only contain digits', minlength: 'SSN must be 9 digits long', maxlength: 'SSN must be 9 digits long', remote: "The SSN you provided already exists"},
					firstName: { required: 'Please enter your first name', maxlength: 'Your first name cannot have more than 255 characters'},
					lastName: { required: 'Please enter your last name', maxlength: 'Your last name cannot have more than 255 characters'},
					phoneNumber: {required: 'Please enter your phone number', digits: 'Phone number can only contain digits'},
					email: { required: 'Please enter your email', email: 'Please enter a valid email', maxlength: 'Your email cannot have more than 255 characters', remote: "The email you provided already exists"},
					password: { required: 'Please enter a password'}
				}
		}
	);

	$('#owner-edit-validation').validate(
		{
			rules:
				{
					ssn: { required: true, digits: true, minlength: 9, maxlength: 9, remote:{url: "/admin/api/edit/owner/checkSSN", type: "post"} },
					firstName: { required: true, maxlength: 255 },
					lastName: {required: true, maxlength: 255 },
					phoneNumber: {required: true, digits: true},
					address: {required: true, maxlength: 255},
					email: { required: true, email: true, maxlength: 255, remote:{url: "/admin/api/edit/owner/checkEmail", type: "post"}},
				},
			messages:
				{
					ssn: { required: 'Please enter your SSN', digits: 'SSN can only contain digits', minlength: 'SSN must be 9 digits long', maxlength: 'SSN must be 9 digits long', remote: "The SSN you provided already exists"},
					firstName: { required: 'Please enter your first name', maxlength: 'Your first name cannot have more than 255 characters'},
					lastName: { required: 'Please enter your last name', maxlength: 'Your last name cannot have more than 255 characters'},
					phoneNumber: {required: 'Please enter your phone number', digits: 'Phone number can only contain digits'},
					address: {required: 'Please enter your address', maxlength: 'Your address cannot have more than 255 characters'},
					email: { required: 'Please enter your email', email: 'Please enter a valid email', maxlength: 'Your email cannot have more than 255 characters', remote: "The email you provided already exists"},
				}
		}
	);

	$('#repair-create-validation').validate(
		{
		rules:
			{
				date: { required: true },
				cost: { required: true, isfloat: true},
				address: { required: true, maxlength: 255 },
				propertyStateId: { required: true, digits: true, minlength: 9, remote:{url: "/admin/api/create/repair/checkStateId", type: "post"} }
			},
		messages:
			{
				date: { required: 'Please enter a date' },
				cost: { required: 'Please enter a cost', isfloat: 'Please enter a valid cost'},
				address: { required: 'Please enter an address', maxlength: 'Address can have up to 255 characters'},
				propertyStateId: { required: 'Please enter the Property State ID', digits: 'ID can only contain digits', minlength: 'ID must be 9 digits long', maxlength: 'ID must be 9 digits long', remote: 'The ID you provided does not exist'}
			}
		}
	);

	$('#repair-edit-validation').validate(
		{
			rules:
				{
					date: { required: true },
					cost: { required: true, isfloat: true},
					address: { required: true, maxlength: 255 },
					propertyStateId: { required: true, digits: true, minlength: 9, remote:{url: "/admin/api/edit/repair/checkStateId", type: "post"} }
				},
			messages:
				{
					date: { required: 'Please enter a date' },
					cost: { required: 'Please enter a cost', isfloat: 'Please enter a valid cost'},
					address: { required: 'Please enter an address', maxlength: 'Address can have up to 255 characters'},
					propertyStateId: { required: 'Please enter the Property State ID', digits: 'ID can only contain digits', minlength: 'ID must be 9 digits long', maxlength: 'ID must be 9 digits long', remote: 'The ID you provided does not exist'}
				}
		}
	);

	$('#search-property-state-id-validation').validate(
		{
			rules:
				{
					propertyStateId: { required: true, digits: true, minlength: 9, maxlength: 9}
				},
			messages:
				{
					propertyStateId: { required: "Please enter a property state ID", digits: 'property state ID must contain only digits', minlength: 'property state ID must have 9 digits', maxlength: 'property state ID must have 9 digits' },}
		}
	);

	$('#search-property-owner-ssn-validation').validate(
		{
			rules:
				{
					ownerSsn: { required: true, digits: true, minlength: 9, maxlength: 9}
				},
			messages:
				{
					ownerSsn: { required: 'Please enter an SSN', digits: 'SSN must contain only digits', minlength: 'SSN must have 9 digits', maxlength: 'SSN must have 9 digits' },
				}
		}
	);

	$('#search-repair-validation').validate(
		{
			rules:
				{
					fromDate: { required: true },
					untilDate: { required: true },
					ownerSsn: { required: true, digits: true, minlength: 9, maxlength: 9 }
				},
			messages:
				{
					fromDate: { required: 'Please enter a starting date' },
					untilDate: { required: 'Please enter an ending date' },
					ownerSsn: { required: 'Please enter an SSN', digits: 'SSN must contain only digits', minlength: 'SSN must have 9 digits', maxlength: 'SSN must have 9 digits' }
				}
		}
	);

	$('#search-customer-ssn-validation').validate(
		{
			rules:
				{
					ssn: { required: true, digits: true, minlength: 9, maxlength: 9}
				},
			messages:
				{
					ssn: { required: "Please enter an SSN", digits: 'SSN must contain only digits', minlength: 'SSN must have 9 digits', maxlength: 'SSN must have 9 digits' },}
		}
	);

	$('#search-customer-email-validation').validate(
		{
			rules:
				{
					email: { required: true, email: true }
				},
			messages:
				{
					email: { required: "Please enter an email", email: 'Please enter a valid email'}
				}
		}
	);

	$('#removePropertyModal').on('show.bs.modal', function (event) {
		const id = event.relatedTarget.dataset.id;
		$('#deleteForm').attr('action', `/admin/remove-property/${id}`);
		$('.modal-title').html('Delete Property');
		$('.modal-body').html('You are about to delete a property with id: '+id+'<br/>All repair jobs from this property will get deleted too.<br/>This action cannot be reversed...<br/>Do you want to proceed?');
	});

	$('#removeCustomerModal').on('show.bs.modal', function (event) {
        const id = event.relatedTarget.dataset.id;
        $('#deleteForm').attr('action', `/admin/remove-customer/${id}`);
        $('.modal-title').html('Delete Customer');
        $('.modal-body').html('You are about to delete a customer with id: '+id+'<br/>All properties and repair jobs from this customer will get deleted too.<br/>This action cannot be reversed...<br/>Do you want to proceed?');
    });

	$('#removeRepairModal').on('show.bs.modal', function (event) {
        const id = event.relatedTarget.dataset.id;
        $('#deleteForm').attr('action', `/admin/remove-repair/${id}`);
        $('.modal-title').html('Delete Repair Job');
        $('.modal-body').html('You are about to delete a repair job with id: '+id+'<br>This action cannot be reversed...<br>Do you want to proceed?');
    });
});