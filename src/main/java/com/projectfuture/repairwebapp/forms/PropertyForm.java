package com.projectfuture.repairwebapp.forms;

import com.projectfuture.repairwebapp.model.PropertyModel;

public class PropertyForm {

    private String[] values;

    public PropertyForm(PropertyModel propertyModel) {
        String propertyStateId = propertyModel.getPropertyStateId().trim();
        String address = propertyModel.getAddress().trim();
        String manufacturingYear = String.valueOf(propertyModel.getManufacturingYear().trim());
        String propertyType = propertyModel.getPropertyType().name();
        String ownerSsn = propertyModel.getOwnerSsn().trim();
        this.values = new String[]{propertyStateId, address, manufacturingYear, propertyType, ownerSsn};
    }

    public String[] getValues(){
        return this.values;
    }

}
