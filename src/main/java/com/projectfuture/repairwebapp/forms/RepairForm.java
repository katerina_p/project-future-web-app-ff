package com.projectfuture.repairwebapp.forms;

import com.projectfuture.repairwebapp.model.RepairModel;

public class RepairForm {

    private String[] values;

    public RepairForm(RepairModel repairModel) {
        String date = repairModel.getDate().trim();
        String status = repairModel.getRepairStatus().name().trim();
        String type = repairModel.getRepairType().name().trim();
        String cost = String.valueOf(repairModel.getCost());
        String stateId = repairModel.getPropertyStateId().trim();
        this.values = new String[]{date,status,type,cost,stateId};
    }

    public String[] getValues(){
        return this.values;
    }

}
