package com.projectfuture.repairwebapp.enums;

public enum PropertyType {
    DEFAULT("Undefined"),
    BLOCK_OF_FLATS("Block of flats"),
    APARTMENT("Apartment"),
    HOUSE("House"),
    MAISONETTE("Maisonette"),
    SHOP("Shop");

    private String propertyType;

    PropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public static String[] names() {
        PropertyType[] types = values();
        String[] names = new String[types.length];
        for (int i = 0; i < types.length; i++) {
            names[i] = types[i].name();
        }
        return names;
    }
}
