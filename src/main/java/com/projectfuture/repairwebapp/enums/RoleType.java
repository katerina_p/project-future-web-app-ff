package com.projectfuture.repairwebapp.enums;

public enum RoleType {

    USER("USER"),
    ADMIN("ADMIN");

    private String roleType;

    RoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getRoleType() {
        return roleType;
    }

    public static String[] names() {
        RoleType[] types = values();
        String[] names = new String[types.length];
        for (int i = 0; i < types.length; i++) {
            names[i] = types[i].name();
        }
        return names;
    }

}
