package com.projectfuture.repairwebapp.enums;

public enum RepairStatus {
    DEFAULT("Pending"),
    PENDING("Pending"),
    UNDERWAY("Underway"),
    COMPLETED("Completed");

    private String repairStatus;

    RepairStatus(String repairStatus) {
        this.repairStatus = repairStatus;
    }

    public String getRepairStatus() {
        return repairStatus;
    }

    public static String[] names() {
        RepairStatus[] types = values();
        String[] names = new String[types.length];
        for (int i = 0; i < types.length; i++) {
            names[i] = types[i].name();
        }
        return names;
    }
}
