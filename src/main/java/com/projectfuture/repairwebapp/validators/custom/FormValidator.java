package com.projectfuture.repairwebapp.validators.custom;

import com.projectfuture.repairwebapp.enums.PropertyType;
import com.projectfuture.repairwebapp.enums.RepairStatus;
import com.projectfuture.repairwebapp.enums.RepairType;
import com.projectfuture.repairwebapp.service.PropertyOwnerService;
import com.projectfuture.repairwebapp.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

@Component
@Configurable
public class FormValidator {

    private String[] values;
    private String res;
    private String[] special;

    @Autowired
    private PropertyOwnerService propertyOwnerService;

    @Autowired
    private PropertyService propertyService;

    public FormValidator() { }

    public FormValidator setValues(String[] values) {
        this.values = values;
        this.special = new String[2];
        return this;
    }

    public FormValidator setRes(String res) {
        this.res = res;
        return this;
    }

    public FormValidator setSpecial(String value){
        this.special[0] = value;
        return this;
    }

    public FormValidator setSpecial(String[] values){
        this.special = values;
        return this;
    }

    private boolean chechDigits(String s){
        for(Character c:s.toCharArray()){
            if(!Character.isDigit(c)){
                return false;
            }
        }
        return true;
    }

    private boolean chechMinLength(String s, int length){
        return s.length() >= length;
    }

    private boolean chechMaxLength(String s, int length){
        return s.length() <= length;
    }

    private boolean chechExactLength(String s, int length){
        return s.length() == length;
    }

    private boolean checkMin(String s, int min){
        return Integer.valueOf(s) >= min;
    }

    private boolean checkMax(String s, int max){
        return Integer.valueOf(s) <= max;
    }

    private boolean checkEmail(String s){
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern pat = Pattern.compile(emailRegex);
        if (s == null) return false;
        return pat.matcher(s).matches();
    }

    private boolean checkFloat(String s){
        String floatRegexPart1 = "^[1-9][0-9]*\\.[0-9]{1,2}$";
        String floatRegexPart2 = "^[1-9][0-9]*$";
        Pattern pat1 = Pattern.compile(floatRegexPart1);
        Pattern pat2 = Pattern.compile(floatRegexPart2);
        if (s == null) return false;
        return pat1.matcher(s).matches() || pat2.matcher(s).matches();
    }

    private boolean checkList(String s, List<String> values){
        return values.contains(s);
    }

    private boolean checkDate(String s) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(s.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    public boolean validate(){
        if(res.equals("create-properties")){
            String propertyStateId = values[0];
            if(propertyStateId == null || !chechDigits(propertyStateId) || !chechExactLength(propertyStateId, 9)){
                return false;
            }else{
                if(propertyService.checkStateId(propertyStateId)) return false;
            }
            String address = values[1];
            if(address == null || !chechMaxLength(address,255)){
                return false;
            }
            String year = values[2];
            if(year == null || !checkMin(year,1900) || !checkMax(year,2020)){
                return false;
            }
            String type = values[3];
            if(type == null || !checkList(type, Arrays.asList(PropertyType.names()))){
                return false;
            }
            String ssn = values[4];
            if(ssn == null || !chechDigits(ssn) || !chechExactLength(ssn,9)){
                return false;
            }else{
                if(!propertyOwnerService.checkSsn(ssn)) return false;
            }
            return true;
        }else if(res.equals("edit-property")){
            String propertyStateId = values[0];
            if(propertyStateId == null || !chechDigits(propertyStateId) || !chechExactLength(propertyStateId, 9)){
                return false;
            }else{
                if(propertyService.checkStateId(propertyStateId) && !this.special[0].equals(propertyStateId)) return false;
            }
            String address = values[1];
            if(address == null || !chechMaxLength(address,255)){
                return false;
            }
            String year = values[2];
            if(year == null || !checkMin(year,1900) || !checkMax(year,2020)){
                return false;
            }
            String type = values[3];
            if(type == null || !checkList(type, Arrays.asList(PropertyType.names()))){
                return false;
            }
            String ssn = values[4];
            if(ssn == null || !chechDigits(ssn) || !chechExactLength(ssn,9)){
                return false;
            }else{
                if(!propertyOwnerService.checkSsn(ssn)) return false;
            }
            return true;
        }else if(res.equals("create-repair") || res.equals("edit-repair")){
            String date = values[0];
            if(date == null || !checkDate(date)){
                return false;
            }
            String status = values[1];
            if(status == null || !checkList(status, Arrays.asList(RepairStatus.names()))){
                return false;
            }
            String type = values[2];
            if(type == null || !checkList(type, Arrays.asList(RepairType.names()))){
                return false;
            }
            String cost = values[3];
            if(cost == null || !checkFloat(cost)){
                return false;
            }
            String stateId = values[4];
            if(stateId == null || !chechDigits(stateId) || !chechExactLength(stateId,9)){
                return false;
            }else{
                if(!propertyService.checkStateId(stateId)) return false;
            }
            return true;
        }else if(res.equals("create-customer")){
            String ssn = values[0];
            if(ssn == null || !chechDigits(ssn) || !chechExactLength(ssn, 9)){
                return false;
            }else {
                if(!propertyOwnerService.checkSsn(ssn)) return false;
            }
            String firstName = values[1];
            if(firstName == null || !chechMaxLength(firstName,255)){
                return false;
            }
            String lastName = values[2];
            if(lastName == null || !chechMaxLength(lastName,255)){
                return false;
            }
            String phone = values[3];
            if(phone == null || !chechDigits(phone)){
                return false;
            }
            String email = values[4];
            if(email == null || !chechMaxLength(email,255) || !checkEmail(email)){
                return false;
            }else{
                if(propertyOwnerService.checkEmail(email)) return false;
            }
            return true;
        }else if(res.equals("edit-customer")){
            String ssn = values[0];
            if(ssn == null || !chechDigits(ssn) || !chechExactLength(ssn, 9)){
                return false;
            }else {
                if(propertyOwnerService.checkSsn(ssn) && !this.special[0].equals(ssn)) return false;
            }
            String firstName = values[1];
            if(firstName == null || !chechMaxLength(firstName,255)){
                return false;
            }
            String lastName = values[2];
            if(lastName == null || !chechMaxLength(lastName,255)){
                return false;
            }
            String phone = values[3];
            if(phone == null || !chechDigits(phone)){
                return false;
            }
            String email = values[4];
            if(email == null || !chechMaxLength(email,255) || !checkEmail(email)){
                return false;
            }else{
                if(propertyOwnerService.checkEmail(email) && !this.special[1].equals(email)) return false;
            }
            return true;
        }
        return false;
    }

}
