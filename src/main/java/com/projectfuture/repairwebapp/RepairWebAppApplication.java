package com.projectfuture.repairwebapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepairWebAppApplication implements CommandLineRunner {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static void main(String[] args) {
		SpringApplication.run(RepairWebAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("=============================");
		logger.info("=====Application Started=====");
		logger.info("=============================");
	}
}
