package com.projectfuture.repairwebapp.repository;

import com.projectfuture.repairwebapp.domain.Property;
import com.projectfuture.repairwebapp.domain.Repair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RepairRepository extends JpaRepository<Repair, Long> {

    List<Repair> findRepairByDate(LocalDate date);

    List<Repair> findRepairByDateBetweenOrderByDate(LocalDate from, LocalDate until);

    List<Repair> findRepairByProperty(Property property);

    List<Repair> findRepairByDateIsAfterOrderByDateAsc(LocalDate date);

    Repair save(Repair repair);

    void deleteRepairById(long id);

}
