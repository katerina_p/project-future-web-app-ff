package com.projectfuture.repairwebapp.repository;

import com.projectfuture.repairwebapp.domain.Property;
import com.projectfuture.repairwebapp.domain.PropertyOwner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PropertyRepository extends JpaRepository<Property, Long> {

    Optional<Property> findPropertyByPropertyStateId(String propertyStateId);

    List<Property> findPropertyByPropertyOwner(PropertyOwner propertyOwner);

    Property save(Property property);

    void deletePropertyById(long id);
}
