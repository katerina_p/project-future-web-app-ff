package com.projectfuture.repairwebapp.repository;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PropertyOwnerRepository extends JpaRepository<PropertyOwner, Long> {

    Optional<PropertyOwner> findPropertyOwnerBySsn(String ssn);

    Optional<PropertyOwner> findPropertyOwnerByEmail(String email);

    Optional<PropertyOwner> findPropertyOwnerByFirstNameAndLastName(String firstName, String lastName);

    PropertyOwner save(PropertyOwner propertyOwner);

    void deleteById(Long id);

}
