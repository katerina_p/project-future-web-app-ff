package com.projectfuture.repairwebapp.mappers;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.enums.RoleType;
import com.projectfuture.repairwebapp.model.PropertyOwnerModel;

public class PropertyOwnerModelToPropertyOwnerMapper {

    public static PropertyOwner mapTo(PropertyOwnerModel propertyOwnerModel){
        PropertyOwner propertyOwner = new PropertyOwner();
        propertyOwner.setSsn(propertyOwnerModel.getSsn());
        propertyOwner.setFirstName(propertyOwnerModel.getFirstName());
        propertyOwner.setLastName(propertyOwnerModel.getLastName());
        propertyOwner.setPhoneNumber(propertyOwnerModel.getPhoneNumber());
        propertyOwner.setEmail(propertyOwnerModel.getEmail());
        propertyOwner.setPassword(propertyOwnerModel.getPassword());
        propertyOwner.setRoleType(RoleType.USER);

        return propertyOwner;
    }
}
