package com.projectfuture.repairwebapp.service;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.enums.RoleType;
import com.projectfuture.repairwebapp.mappers.PropertyOwnerModelToPropertyOwnerMapper;
import com.projectfuture.repairwebapp.mappers.PropertyOwnerToPropertyOwnerModelMapper;
import com.projectfuture.repairwebapp.model.PropertyOwnerModel;
import com.projectfuture.repairwebapp.repository.PropertyOwnerRepository;
import com.projectfuture.repairwebapp.utils.GlobalAttributes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Configurable
public class PropertyOwnerServiceImpl implements PropertyOwnerService{

    @Autowired
    private PropertyOwnerRepository propertyOwnerRepository;

    @Autowired
    private PropertyOwnerToPropertyOwnerModelMapper mapper;

    @Override
    public PropertyOwner findPropertyOwner(Long id) {
        return propertyOwnerRepository.findById(id).get();
    }

    @Override
    public List<PropertyOwner> getAllUserPropertyOwners() {
        List<PropertyOwner> propertyOwners = propertyOwnerRepository.findAll();
        propertyOwners.removeIf(o -> o.getRole().equals(RoleType.ADMIN));
        return propertyOwners;
    }

    public boolean checkSsn(String ssn){
        try {
            findPropertyOwnerBySsn(ssn);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean checkSsn(String ssn, String session) {
        if(GlobalAttributes.ssns.get(session).equals(ssn)){
            return true;
        }
        return !checkSsn(ssn);
    }

    @Override
    public PropertyOwner findPropertyOwnerBySsn(String ssn) {
        return propertyOwnerRepository.findPropertyOwnerBySsn(ssn).get();
    }

    @Override
    public boolean checkEmail(String email){
        try {
            findPropertyOwnerByEmail(email);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean checkEmail(String email, String session) {
        if(GlobalAttributes.emails.get(session).equals(email)){
            return true;
        }
        return !checkEmail(email);
    }

    @Override
    public PropertyOwner findPropertyOwnerByEmail(String email) {
        return propertyOwnerRepository.findPropertyOwnerByEmail(email).get();
    }

    @Override
    public PropertyOwner findPropertyOwnerByFirstNameAndLastName(String firstName, String lastName) {
        return propertyOwnerRepository.findPropertyOwnerByFirstNameAndLastName(firstName, lastName).get();
    }

    @Override
    public PropertyOwner createPropertyOwner(PropertyOwnerModel propertyOwnerModel) {
        PropertyOwner newPropertyOwner = PropertyOwnerModelToPropertyOwnerMapper.mapTo(propertyOwnerModel);

        return propertyOwnerRepository.save(newPropertyOwner);
    }

    @Override
    public PropertyOwner updatePropertyOwner(PropertyOwnerModel propertyOwnerModel) {
        PropertyOwner originalPropertyOwner = PropertyOwnerModelToPropertyOwnerMapper.mapTo(propertyOwnerModel);
        originalPropertyOwner.setId(propertyOwnerModel.getId());
        return propertyOwnerRepository.save(originalPropertyOwner);
    }

    @Override
    public void deleteById(Long id) {
        propertyOwnerRepository.deleteById(id);
    }
}
