package com.projectfuture.repairwebapp.service;

import com.projectfuture.repairwebapp.domain.Property;
import com.projectfuture.repairwebapp.model.PropertyModel;

import java.util.List;


public interface PropertyService {

    Property findProperty(Long id);

    List<Property> getAllProperties();

    List<Property> findPropertyByPropertyOwnerSsn(String ssn);

    List<Property> findPropertyByPropertyOwnerEmail(String email);

    boolean checkStateId(String id);

    Property findPropertyByPropertyStateId(String propertyStateId);

    Property createProperty(PropertyModel propertyModel);

    Property updateProperty(PropertyModel propertyModel);

    void deleteProperty(Long id);
}
