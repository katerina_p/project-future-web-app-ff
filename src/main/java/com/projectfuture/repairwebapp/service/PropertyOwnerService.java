package com.projectfuture.repairwebapp.service;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.model.PropertyOwnerModel;
import java.util.List;

public interface PropertyOwnerService {

    PropertyOwner findPropertyOwner(Long id);

    List<PropertyOwner> getAllUserPropertyOwners();

    boolean checkSsn(String ssn);

    boolean checkSsn(String ssn, String id);

    PropertyOwner findPropertyOwnerBySsn(String ssn);

    public boolean checkEmail(String email);

    public boolean checkEmail(String email, String session);

    PropertyOwner findPropertyOwnerByEmail(String email);

    PropertyOwner findPropertyOwnerByFirstNameAndLastName(String firstName, String lastName);

    PropertyOwner createPropertyOwner(PropertyOwnerModel propertyOwnerModel);

    PropertyOwner updatePropertyOwner(PropertyOwnerModel propertyOwnerModel);

    void deleteById(Long id);
}
