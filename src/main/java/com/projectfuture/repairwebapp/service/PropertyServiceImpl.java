package com.projectfuture.repairwebapp.service;

import com.projectfuture.repairwebapp.domain.Property;
import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.mappers.PropertyModelToProperty;
import com.projectfuture.repairwebapp.model.PropertyModel;
import com.projectfuture.repairwebapp.repository.PropertyOwnerRepository;
import com.projectfuture.repairwebapp.repository.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PropertyServiceImpl implements PropertyService{

    @Autowired
    private PropertyRepository propertyRepository;

    @Autowired
    private PropertyOwnerRepository propertyOwnerRepository;

    @Override
    public Property findProperty(Long id) {
        return propertyRepository.findById(id).get();
    }

    @Override
    public List<Property> getAllProperties() {
        return propertyRepository.findAll();
    }

    @Override
    public List<Property> findPropertyByPropertyOwnerSsn(String ssn) {
        return propertyRepository.findPropertyByPropertyOwner(propertyOwnerRepository.findPropertyOwnerBySsn(ssn).get());
    }

    public boolean checkStateId(String id){
        try {
            findPropertyByPropertyStateId(id);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public List<Property> findPropertyByPropertyOwnerEmail(String email) {
        PropertyOwner propertyOwner = propertyOwnerRepository.findPropertyOwnerByEmail(email).get();
        List<Property> properties = propertyRepository.findPropertyByPropertyOwner(propertyOwner);
        return properties;
    }

    @Override
    public Property findPropertyByPropertyStateId(String propertyStateId) {
        return propertyRepository.findPropertyByPropertyStateId(propertyStateId).get();
    }

    @Override
    public Property createProperty(PropertyModel propertyModel) {
        Property property = PropertyModelToProperty.mapTo(propertyModel);
        return propertyRepository.save(property);
    }

    @Override
    public Property updateProperty(PropertyModel propertyModel) {
        Property property = PropertyModelToProperty.mapTo(propertyModel);;
        property.setId(propertyModel.getId());

        return propertyRepository.save(property);
    }

    @Override
    public void deleteProperty(Long id) {
        propertyRepository.deletePropertyById(id);
    }
}
