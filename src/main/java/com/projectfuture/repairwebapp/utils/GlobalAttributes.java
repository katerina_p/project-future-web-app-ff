package com.projectfuture.repairwebapp.utils;

import java.util.HashMap;
import java.util.Map;

public class GlobalAttributes {
    public static Map<String, String> emails = new HashMap<>();
    public static Map<String, String> ssns = new HashMap<>();
}
