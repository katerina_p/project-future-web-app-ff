package com.projectfuture.repairwebapp.model;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.enums.PropertyType;

public class PropertyModel {

    private long id;
    private String propertyStateId;
    private String address;
    private String manufacturingYear;
    private PropertyType propertyType;
    private String ownerSsn;
    private PropertyOwner propertyOwner;

    public PropertyModel() {
    }

    public PropertyModel(long id, String propertyStateId, String address, String manufacturingYear, PropertyType propertyType, String ownerSsn, PropertyOwner propertyOwner) {
        this.id = id;
        this.propertyStateId = propertyStateId;
        this.address = address;
        this.manufacturingYear = manufacturingYear;
        this.propertyType = propertyType;
        this.ownerSsn = ownerSsn;
        this.propertyOwner = propertyOwner;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPropertyStateId() {
        return propertyStateId;
    }

    public void setPropertyStateId(String propertyStateId) {
        this.propertyStateId = propertyStateId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getManufacturingYear() {
        return manufacturingYear;
    }

    public void setManufacturingYear(String manufacturingYear) {
        this.manufacturingYear = manufacturingYear;
    }

    public PropertyType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType) {
        this.propertyType = propertyType;
    }

    public String getOwnerSsn() {
        return ownerSsn;
    }

    public void setOwnerSsn(String ownerSsn) {
        this.ownerSsn = ownerSsn;
    }

    public PropertyOwner getPropertyOwner() {
        return propertyOwner;
    }

    public void setPropertyOwner(PropertyOwner propertyOwner) {
        this.propertyOwner = propertyOwner;
    }
}
