package com.projectfuture.repairwebapp.domain;

import com.projectfuture.repairwebapp.enums.RoleType;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="PROPERTY_OWNER")
public class PropertyOwner {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="owner_id")
    private Long id;

    @Column(name="owner_SSN", nullable = false, unique=true)
    private String ssn;

    @Column(name="first_name", nullable = false)
    private String firstName;

    @Column(name="last_name", nullable = false)
    private String lastName;

    @Column(name="phone_number", nullable = false, unique=true)
    private String phoneNumber;

    @Column(name="email", nullable = false, unique=true)
    private String email;

    @Column(name="password", nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name="role", nullable = false, columnDefinition = "VARCHAR(45)")
    private RoleType roleType;

    @OneToMany(mappedBy = "propertyOwner", targetEntity = Property.class, cascade = CascadeType.REMOVE)
    private List<Property> properties;

    public PropertyOwner(String ssn, String firstName, String lastName, String phoneNumber, String email, String password, List<Property> properties, RoleType roleType) {
        this.ssn = ssn;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.properties = properties;
        this.roleType = roleType;
    }

    public PropertyOwner() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername(){return email;}

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setRepairs(List<Property> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Property owner{");
        sb.append("id=").append(id);
        sb.append(", SSN=").append(ssn);
        sb.append(", first name=").append(firstName);
        sb.append(", last name=").append(lastName);
        sb.append(", phone number=").append(phoneNumber);
        sb.append(", email=").append(email);
        sb.append(", password=").append(password);
        sb.append('}');
        return sb.toString();
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public RoleType getRole() {
        return this.roleType;
    }
}
