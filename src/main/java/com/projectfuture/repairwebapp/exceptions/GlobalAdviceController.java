package com.projectfuture.repairwebapp.exceptions;

import com.projectfuture.repairwebapp.utils.GlobalAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.ErrorManager;

@ControllerAdvice(basePackages = {
        "com.projectfuture.repairwebapp"
})
public class GlobalAdviceController {

    private final Logger LOG =  LoggerFactory.getLogger(this.getClass()); ;

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleException(final Exception exception,
                                  final HttpServletRequest request, final HttpServletResponse response) {
        request.setAttribute("errorMessage", "An internal server error has occured... Please try again later.");
        LoggerFactory.getLogger(this.getClass()).error("An exception occured...", exception);

        return "error";
    }
}
