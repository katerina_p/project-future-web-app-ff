package com.projectfuture.repairwebapp.controller.api;

import com.projectfuture.repairwebapp.service.PropertyOwnerService;
import com.projectfuture.repairwebapp.service.PropertyService;
import com.projectfuture.repairwebapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("admin/api")
public class ApiController {

    @Autowired
    private PropertyOwnerService propertyOwnerService;

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private RepairService repairService;

    //PROPERTIES
    @PostMapping({"/create/property/checkSSN","/edit/property/checkSSN"})
    public String SSNExists1(@RequestParam("ownerSsn") String ssn){
        return String.valueOf(propertyOwnerService.checkSsn(ssn));
    }
    @PostMapping(path="/create/property/checkStateId")
    public String StateIdExists1(@RequestParam("propertyStateId") String id){
        return String.valueOf(!propertyService.checkStateId(id));
    }

    //PROPERTY OWNERS
    @PostMapping(path="/create/owner/checkSSN")
    public String SSNExists2(@RequestParam("ssn") String ssn){
        return String.valueOf(!propertyOwnerService.checkSsn(ssn));
    }
    @PostMapping(path="/create/owner/checkEmail")
    public String EmailExists(@RequestParam("email") String email){
        return String.valueOf(!propertyOwnerService.checkEmail(email));
    }
    @PostMapping(path="/edit/owner/checkEmail")
    public String EmailExistsEdit(@RequestParam("email") String email, HttpServletRequest req){
        return String.valueOf(propertyOwnerService.checkEmail(email, req.getSession(false).getId()));
    }
    @PostMapping(path="/edit/owner/checkSSN")
    public String SSNExists2(@RequestParam("ssn") String ssn, HttpServletRequest req){
        return String.valueOf(propertyOwnerService.checkSsn(ssn, req.getSession(false).getId()));
    }

    //REPAIRS
    @PostMapping({"/create/repair/checkStateId","edit/repair/checkStateId"})
    public String StateIdExists2(@RequestParam("propertyStateId") String id){
        return String.valueOf(propertyService.checkStateId(id));
    }
}
