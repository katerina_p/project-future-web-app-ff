package com.projectfuture.repairwebapp.controller.page;

import com.projectfuture.repairwebapp.domain.Repair;
import com.projectfuture.repairwebapp.forms.RepairForm;
import com.projectfuture.repairwebapp.model.RepairModel;
import com.projectfuture.repairwebapp.service.PropertyService;
import com.projectfuture.repairwebapp.service.RepairService;
import com.projectfuture.repairwebapp.validators.custom.FormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class RepairController {

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private RepairService repairService;

    @Autowired
    private FormValidator validator;

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");


    @GetMapping("/repairs")
    public String getRepairs(Model model){
        List<Repair> repairs = repairService.getAllRepairs();
        model.addAttribute("repairs", repairs);
        return "pages/admin/repair/list";
    }

    @GetMapping({"/create-repair"})
    public String getRepairCreatePage(Model model){
        return "pages/admin/repair/create";
    }

    @PostMapping({"/create-repair"})
    public String saveRepair(RepairModel repairModel){
        if(validator.setRes("create-repair").setValues(new RepairForm(repairModel).getValues()).validate()){
            String propertyStateId = repairModel.getPropertyStateId();
            repairModel.setProperty(propertyService.findPropertyByPropertyStateId(propertyStateId));
            repairService.createRepair(repairModel);
            return "redirect:/admin/repairs";
        }else{
            return "redirect:/admin/home";
        }
    }

    @GetMapping({"/edit-repair/{id}"})
    public String getRepairEditPage(@PathVariable Long id, Model model){
        try{
            Repair repair = repairService.findRepair(id);
            model.addAttribute("repair", repair);
            return "pages/admin/repair/edit";
        }catch (Exception e){
            return "redirect:/admin/home";
        }
    }

    @PostMapping("/edit-repair/{id}")
    public String updateRepair(@PathVariable Long id, RepairModel repairModel){
        if(validator.setRes("edit-repair").setValues(new RepairForm(repairModel).getValues()).validate()) {
            String propertyStateId = repairModel.getPropertyStateId();
            repairModel.setProperty(propertyService.findPropertyByPropertyStateId(propertyStateId));
            repairService.updateRepair(repairModel);
            return "redirect:/admin/repairs";
        }else{
            return "redirect:/admin/home";
        }
    }

    @PostMapping({"/remove-repair/{id}"})
    public String deleteRepair(@PathVariable Long id){
        repairService.deleteRepairById(id);
        return "redirect:/admin/repairs";
    }

    @GetMapping({"/search-repair"})
    public String getRepairsSearchPage(){
        return "pages/admin/repair/search";
    }

    @PostMapping({"/search-repair"})
    public String getRepairSearchResult(@RequestParam(name = "fromDate") String fromDateString,
                                  @RequestParam(name = "untilDate") String untilDateString,
                                  @RequestParam(name = "ownerSsn") String ownerSsn,
                                  Model model){
        try {
            LocalDate fromDate = LocalDate.parse(fromDateString, DATE_TIME_FORMATTER);
            LocalDate untilDate = LocalDate.parse(untilDateString, DATE_TIME_FORMATTER);
            List<Repair> repairsByOwnerSsn = repairService.findRepairByPropertyOwnerSsn(ownerSsn);
            List<Repair> repairsByDate = repairService.findRepairByDateBetweenOrderByDate(fromDate, untilDate);

            List<Repair> repairs = new ArrayList<>();

            for (Repair r : repairsByDate) {
                if (repairsByOwnerSsn.contains(r)) {
                    repairs.add(r);
                }
            }

            if (!repairs.isEmpty()) {
                model.addAttribute("repairs", repairs);
            }
            model.addAttribute("fromDate", fromDateString);
            model.addAttribute("untilDate", untilDateString);
            model.addAttribute("ownerSsn", ownerSsn);
        }catch (Exception e){
            model.addAttribute("no_results","No results found...");
        }
        return "pages/admin/repair/search";
    }
}
