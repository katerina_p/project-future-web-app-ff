package com.projectfuture.repairwebapp.controller.page;

import com.projectfuture.repairwebapp.forms.LoginForm;
import com.projectfuture.repairwebapp.validators.LoginValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    private static final String LOGIN_FORM = "form-login";

    @Autowired
    private LoginValidator loginValidator;

    @InitBinder(LOGIN_FORM)
    protected void initBinder(final WebDataBinder binder) {
        binder.addValidators(loginValidator);
    }

    @GetMapping("/login")
    public String login(@RequestParam(name = "error", required = false) String error, Model model) {

        Authentication authContext = SecurityContextHolder.getContext().getAuthentication();

        if (!(authContext instanceof AnonymousAuthenticationToken)) {
            if (authContext.getAuthorities().stream().anyMatch(auth -> auth.getAuthority().equals("ADMIN"))) {
                return "redirect:/admin/home";
            }
            return "redirect:/user/home";
        }
        model.addAttribute(LOGIN_FORM, new LoginForm());
        if(error != null){
            model.addAttribute("error", "The credentials you provided are not correct...");
        }
        return "pages/login";
    }

}
