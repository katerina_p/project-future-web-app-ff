package com.projectfuture.repairwebapp.controller.page;

import com.projectfuture.repairwebapp.domain.PropertyOwner;
import com.projectfuture.repairwebapp.forms.PropertyOwnerForm;
import com.projectfuture.repairwebapp.model.PropertyOwnerModel;
import com.projectfuture.repairwebapp.security.SecurityConfig;
import com.projectfuture.repairwebapp.service.PropertyOwnerService;
import com.projectfuture.repairwebapp.utils.GlobalAttributes;
import com.projectfuture.repairwebapp.validators.custom.FormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class CustomerController {

    @Autowired
    private SecurityConfig securityConfig;

    @Autowired
    private PropertyOwnerService propertyOwnerService;

    @Autowired
    private FormValidator validator;

    @GetMapping("/customers")
    public String getCustomers(Model model){
        List<PropertyOwner> propertyOwners = propertyOwnerService.getAllUserPropertyOwners();
        model.addAttribute("owners", propertyOwners);
        return "pages/admin/customer/list";
    }

    @GetMapping({"/create-customer"})
    public String getCustomerCreatePage(Model model){
        PropertyOwner newPropertyOwner = new PropertyOwner();
        model.addAttribute("propertyOwner", newPropertyOwner);
        return "pages/admin/customer/create";
    }

    @PostMapping ({"/create-customer"})
    public String createNewCustomer(PropertyOwnerModel model) {
        if (validator.setRes("create-customer").setValues(new PropertyOwnerForm(model).getValues()).validate()){
            model.setPassword(securityConfig.passwordEncoder().encode(model.getPassword()));
            propertyOwnerService.createPropertyOwner(model);
            return "redirect:/admin/customers";
        }else{
            return "redirect:/admin/home";
        }
    }

    @GetMapping({"/edit-customer/{id}"})
    public String getCustomerEditPage(@PathVariable Long id, Model model, HttpServletRequest req){
        try{
            PropertyOwner owner = propertyOwnerService.findPropertyOwner(id);
            model.addAttribute("owner", owner);
            GlobalAttributes.emails.put(req.getSession(false).getId(),owner.getEmail());
            GlobalAttributes.ssns.put(req.getSession(false).getId(),owner.getSsn());
            return "pages/admin/customer/edit";
        }catch (Exception e){
            return "redirect:/admin/home";
        }
    }

    @PostMapping({"/edit-customer/{id}"})
    public String returnFromCustomerEditPage(@PathVariable Long id, PropertyOwnerModel model, HttpServletRequest req){
        PropertyOwner owner = propertyOwnerService.findPropertyOwner(id);
        String[] info = new String[]{owner.getSsn(), owner.getEmail()};
        if (validator.setRes("edit-customer").setValues(new PropertyOwnerForm(model).getValues()).setSpecial(info).validate()) {
            model.setId(id);
            if (model.getPassword() != null) {
                model.setPassword(securityConfig.passwordEncoder().encode(model.getPassword()));
            } else {
                model.setPassword(propertyOwnerService.findPropertyOwner(id).getPassword());
            }
            propertyOwnerService.updatePropertyOwner(model);
            GlobalAttributes.emails.remove(req.getSession(false).getId());
            GlobalAttributes.ssns.remove(req.getSession(false).getId());
            return "redirect:/admin/customers";
        }else{
            return "redirect:/admin/home";
        }
    }

    @PostMapping({"/remove-customer/{id}"})
    public String getCustomerRemovePage(@PathVariable Long id){
        propertyOwnerService.deleteById(id);
        return "redirect:/admin/customers";
    }

    @GetMapping({"/search-customers"})
    public String getSearchCustomerPage(){
        return "pages/admin/customer/search";
    }

    @PostMapping({"/search-customers"})
    public String getCustomerSearchResult(@RequestParam(name = "ssn") String ssn,
                                          @RequestParam(name = "email") String email,
                                          Model model){
        try{
            PropertyOwner owner = null;
            if (!ssn.equals("")){
                owner = propertyOwnerService.findPropertyOwnerBySsn(ssn);
            }else if (!email.equals("")){
                owner = propertyOwnerService.findPropertyOwnerByEmail(email);
            }
            if(!owner.equals(null)){
                model.addAttribute("owner", owner);
            }
            model.addAttribute("ssn",ssn);
            model.addAttribute("email",email);
        }catch(Exception e){
            model.addAttribute("no_results","No results found...");
        }
        return "pages/admin/customer/search";
    }


}
