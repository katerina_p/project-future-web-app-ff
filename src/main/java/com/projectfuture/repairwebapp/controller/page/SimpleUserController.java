package com.projectfuture.repairwebapp.controller.page;

import com.projectfuture.repairwebapp.domain.Property;
import com.projectfuture.repairwebapp.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/user")
public class SimpleUserController {

    @Autowired
    private PropertyService propertyService;

    @GetMapping("/properties")
    public String getRepairs(Model model, HttpServletRequest request){
        String email = request.getSession().getAttribute("username").toString();
        List<Property> properties = propertyService.findPropertyByPropertyOwnerEmail(email);
        model.addAttribute("properties", properties);
        return "pages/user/property/list";
    }

}
